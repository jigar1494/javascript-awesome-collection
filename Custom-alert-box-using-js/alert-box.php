<?php
/**
 * Created by PhpStorm.
 * User: Jigar
 * Date: 03-Mar-16
 * Time: 7:00 PM
 */
?>
<html>
<head>
    <style type="text/css">
        #dialogoverlay{
            display: none;
            opacity: .8;
            position: fixed;
            top: 0px;
            left: 0px;
            background: #FFFFFF;
            width: 100%;
            z-index: 10;
        }
        #dialogbox{
            display: none;
            position: fixed;
            background: #000;
            border-radius: 7px;
            width: 550px;
            z-index: 10;
        }
        #dialogbox > div{background: #FFF;margin: 8px;}
        #dialogbox > div > #dialogboxhead{background: #666;font-size: 19px;padding: 10px;color:#CCC;}
        #dialogbox > div >#dialogboxbody{background: #333;padding: 20px;color:#FFF;}
        #dialogbox > div >#dialogboxfoot{background: #666;padding: 10px;text-align: right;}


    </style>
    <script>
        function customalert(){
            this.render=function(){
                var winw=window.innerWidth;
                var winh=window.innerHeight;
                var dialogoverlay=document.getElementById('dialogoverlay');
                var dialogbox=document.getElementById('dialogbox');
                dialogoverlay.style.display="block";
                dialogoverlay.style.height=winh+"px";
            }
            this.ok=function() {

            }

        }

        var alert=new customalert();
    </script>

</head>
<body>
<div id="dialogoverlay"></div>
<div id="dialogbox">
    <div>
    <div id="dialogboxhead"></div>
    <div id="dialogboxbody"></div>
    <div id="dialogboxfoot"></div>
    </div>
</div>
<h1>My web content...</h1>
<h2>My web content...</h2>
<button onclick="alert("Hello...Jigs..!");">Custom alert</button>
</body>

</html>
